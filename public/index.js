var animals = ["go abseiling", "go swimming", "go diving", "go climbing", "go trampolining", "do archery", "do gymnastics", "do athletics", "play badminton", "play table tennis", "play hockey", "play ice hockey", "play football", "play basketball", "go snowboarding", "go skiing", "go skating"];
var length = animals.length;
var currentPosition = 0;
var speech = null;

initializeItem();
showNavigationButtons();
loadListeners();

function initializeItem()
{
    loadImage();
    loadText();
    loadSpeech();
}

function loadImage()
{
    document.querySelector(".image").src="img/" + animals[currentPosition].toLowerCase() +".jpg";
}

function loadText()
{
    document.querySelector(".text").textContent=animals[currentPosition];
}

function loadSpeech()
{
    speech = new SpeechSynthesisUtterance(animals[currentPosition]);
    speech.lang = 'en-UK';
    document.querySelector(".fa-volume-up").addEventListener("click", playSpeech);
}

function showNavigationButtons()
{
    var fas=document.querySelectorAll('.fas');
    fas.forEach(f => f.style.display="block");
}

function loadListeners()
{
    document.querySelector(".fa-angle-right").addEventListener("click", nextElement);
    document.querySelector(".fa-angle-left").addEventListener("click", previousElement);
}

function playSpeech() {
    window.speechSynthesis.speak(speech);
}

function nextElement() {
    if (currentPosition < (length-1)) {
        currentPosition++;
    }
    else {
        currentPosition = 0;
    }
    initializeItem();
}

function previousElement() {
    if (currentPosition > 0)
    {
        currentPosition--;
    }
    else {
        currentPosition = (length-1);
        console.log(currentPosition);
    }
    initializeItem();
}
